
resource "aws_lb_listener" "this" {
  load_balancer_arn = "arn:aws:elasticloadbalancing:eu-west-2:713263874451:loadbalancer/net/pgbouncer-ecs-prod-nlb/81dd0e7aa60ac3da"
  port              = var.service_port
  protocol          = "TCP"
  tags = (merge(
    "${var.tags}")
  )
  tags_all = {}

  default_action {
    order            = 0
    target_group_arn = data.aws_lb_target_group.this.arn
    type             = "forward"
  }

}

resource "aws_lb_target_group" "this" {
  connection_termination = false
  deregistration_delay   = "300"
  name                   = var.target_group_name
  port                   = var.service_port
  preserve_client_ip     = "true"
  protocol               = "TCP"
  proxy_protocol_v2      = false
  tags = (merge(
    "${var.tags}")
  )
  tags_all    = {}
  target_type = "instance"
  vpc_id      = "vpc-09057d75129f950e1"

  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    port                = "traffic-port"
    protocol            = "TCP"
    timeout             = 10
    unhealthy_threshold = 3
  }

  stickiness {
    cookie_duration = 0
    enabled         = false
    type            = "source_ip"
  }
}

resource "aws_ecs_service" "this" {
  cluster                            = "arn:aws:ecs:eu-west-2:713263874451:cluster/pgbouncer-prod-ecs"
  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 0
  desired_count                      = 3
  enable_ecs_managed_tags            = true
  enable_execute_command             = false
  health_check_grace_period_seconds  = 0
  iam_role                           = "aws-service-role"
  launch_type                        = "EC2"
  name                               = var.aws_ecs_service_name
  propagate_tags                     = "NONE"
  scheduling_strategy                = "REPLICA"
  tags = (merge(
    "${var.tags}")
  )
  tags_all        = {}
  task_definition = "${var.task_definition_name}:1"

  deployment_circuit_breaker {
    enable   = false
    rollback = false
  }

  deployment_controller {
    type = "ECS"
  }

  load_balancer {
    container_name   = var.task_definition_name
    container_port   = 6432
    target_group_arn = aws_lb_target_group.this.arn
  }

  ordered_placement_strategy {
    field = "attribute:ecs.availability-zone"
    type  = "spread"
  }
  ordered_placement_strategy {
    field = "instanceId"
    type  = "spread"
  }
}

resource "aws_ecs_task_definition" "this" {
  container_definitions = jsonencode(
    [
      {
        cpu = 0
        environment = [
          {
            name  = "DBDNSRW"
            value = var.postgresql_ip
          },
          {
            name  = "DBNAME"
            value = "3commas"
          },
          {
            name  = "DBPASSWD"
            value = "JBrRNwrKNkvdfn33"
          },
          {
            name  = "DBUSER"
            value = "postgres-exporter"
          },
        ]
        essential         = true
        image             = var.image
        memoryReservation = 2048
        mountPoints       = []
        name              = var.task_definition_name
        portMappings = [
          {
            containerPort = 6432
            hostPort      = var.service_port
            protocol      = "tcp"
          },
          {
            containerPort = 9127
            hostPort      = var.exporter_port
            protocol      = "tcp"
          },
        ]
        volumesFrom = []
      },
    ]
  )
  requires_compatibilities = [
    "EC2",
  ]
  revision = 1
  tags = (merge(
    "${var.tags}")
  )
  tags_all = {}
}