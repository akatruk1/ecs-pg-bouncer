RDS example:

```
module "rds" {
  source               = "git::git@gitlab.com:akatruk1/ecs-pg-bouncer.git"
  target_group_name    = pgbouncer-ecs-6109-prod-nlb
  service_port         = 6109
  exporter_port        = 9109
  postgresql_ip        = 10.19.137.128
  task_definition_name = pg-bouncer-pg-5-6109-production
  aws_ecs_service_name = pg-9-production-6109-9109
  image                = "713263874451.dkr.ecr.eu-west-2.amazonaws.com/pg_bouncer:pg-9"
  

  tags = {
    project   = "test-project"
    owner     = "DevOps team"
    app-owner = "Andrei Katruk"
  }

```