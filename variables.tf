variable "target_group_name" {}
variable "service_port" {}
variable "exporter_port" {}
variable "postgresql_ip" {}
variable "task_definition_name" {}
variable "aws_ecs_service_name" {}
variable "image" {}
variable "tags" {}